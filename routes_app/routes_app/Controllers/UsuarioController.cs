﻿using System;
using Helper;
using routes_app.Models;
using routes_app.filters;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace routes_app.Controllers
{
    public class UsuarioController : Controller
    {
        //
        // GET: /Usuario/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Guardar(Usuario model, HttpPostedFileBase Foto, int CodUsr)
        {
            
            Permisos permiso = new Permisos();

            var rm = new ResponseModel();

            ModelState.Remove("Password");

            if (ModelState.IsValid)
            {
                rm = model.Guardar(Foto);


                //guardar el permiso : 1 porque es editar
                rm = permiso.Actualizar(CodUsr, model.IdUsuario);
            }

            
            
             

            return Json(rm);
        }
	}
}