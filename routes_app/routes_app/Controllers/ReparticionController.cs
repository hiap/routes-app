﻿using routes_app.Models;
using routes_app.filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa.MVC;

namespace routes_app.Controllers
{
    public class ReparticionController : Controller
    {
        //
        // GET: /Reparticion/
        private Reparticion nreparticion = new Reparticion();
        private Usuario usuario = new Usuario();
        private Instruccion instruccion = new Instruccion();
        private Accion accion = new Accion();
        private Archivos archivos = new Archivos();
        private Ruta ruta = new Ruta();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult eliminarArchivo(int ID)
        {
            Archivos archivo = archivos.Obtener(ID);
            Accion naccion = accion.ObtenerPorIdAccion(archivo.IdAccion);
            int codReparticion = naccion.IdReparticion;
            int nivel = Convert.ToInt32(archivo.tipo);
            string ruta = "";

            //eliminar archivo
            ResponseModel rm = archivos.EliminarArchivo(archivo);

            if (nivel == 1) ruta = "~/reparticion/ActualizarReparticion/";
                
           return Redirect( ruta + codReparticion);  
        }
        public JsonResult subirArchivo(HttpPostedFileBase CargaArchivo, int Nivel, int codDoc)
        {
           var rm = new ResponseModel();
           Archivos archivo = new Archivos();
           if (Nivel == 1)
           {
               //es para Direccion
               archivo.tipo = "1";
               archivo.IdAccion = codDoc;
               rm = archivo.cargarArchivo(CargaArchivo, archivo); 

               if(rm.response)
               {
                   rm.href = Url.Content("self");
               }

           }
            
           if (Nivel == 2)
           {
               //es para Direccion
               archivo.tipo = "2";
               archivo.IdAccion = codDoc;
               rm = archivo.cargarArchivo(CargaArchivo, archivo);

               if (rm.response)
               {
                   rm.href = Url.Content("self");
               }
           }
           
             
                //rm = model.Guardar(Foto);
            return Json(rm);
        }
        public JsonResult Guardar(Reparticion model, int instruccionSelect = 0, int actualizaAccion = 0)
        {
            var rm = new ResponseModel();

            Usuario usr = usuario.Obtener(Convert.ToInt32(model.CodUsrOrig));
                model.NomUsrOrig = usr.Nombre;

                usr = usuario.Obtener(Convert.ToInt32(model.CodUsrDest));
                model.NomUsrDest = usr.Nombre;

                

            if (instruccionSelect == 0)
            {
                //es nueva reparticion sin ACCION
                model.Estado = "Pendiente";
                if (ModelState.IsValid)
                {
                    rm = model.Guardar();

                    if (rm.response)
                    {
                        rm.href = Url.Content("~/reparticion/EditarReparticion/" + model.IdReparticion);
                    }

                }
            }else
            {
                if (instruccionSelect >0)
                {
                    model.Estado = "Pendiente";
                    //es una instruccion Válida
                    if (ModelState.IsValid)
                    {
                        //actualizar ell modelo Reparticion
                        rm = model.Guardar();

                        if (rm.response)
                        {

                            rm.SetResponse(false);

                            //crear una nueva accion
                            if (actualizaAccion == 0)
                            {
                                Accion naccion = new Accion();
                                naccion.IdAccion = 0;
                                naccion.IdRuta = model.IdRuta;
                                naccion.IdReparticion = model.IdReparticion;
                                naccion.IdInstruccion = instruccionSelect;
                                naccion.Estadp = "Pendiente";

                                rm = naccion.Guardar();
                            }
                            else
                            {
                                Accion naccion = accion.Obtener(model.IdReparticion);
                                naccion.IdInstruccion = instruccionSelect;

                                rm = naccion.Guardar();
                            }

                            

                            if (rm.response)
                            {
                                rm.href = Url.Content("~/ruta/EditarNuevaHojaRuta/" + model.IdRuta);
                            }
                            
                            
                        }

                    }
                }
                else rm.message = "Debe seleccionar una ACCION válida."; 
            }
            return Json(rm);
        }
        [Autenticado]
        public ActionResult ActualizarReparticion(int Id)
        {
            ViewBag.UsuariosListados = usuario.Listar();
            ViewBag.instruccionLista = instruccion.Listar();
            Reparticion nrepart = nreparticion.Obtener(Id);

            Accion acciones = accion.Obtener(Id);
            ViewBag.naccion = acciones;

            ViewBag.codInstruccion = accion.ObtenerIdInstruccion(nrepart.IdReparticion);

            ViewBag.carpeta = archivos.Listar(acciones.IdAccion);
            
            return View(nrepart);

        }
        [Autenticado]
        public ActionResult EditarReparticion(int Id, int nuevo = 0)
        {
            ViewBag.UsuariosListados = usuario.Listar();
            ViewBag.instruccionLista = instruccion.Listar();
            
            Reparticion nrepart = nreparticion.Obtener(Id);
            
            return View(nrepart);
        }

        [Autenticado]
        public ActionResult EntregarReparticion(int Id, int nuevo = 0)
        {
            ViewBag.UsuariosListados = usuario.Listar();
            Accion accionUsr = accion.Obtener(Id);
            ViewBag.accionReparticion = accionUsr;

            

            Reparticion nrepart = nreparticion.Obtener(Id);

            Ruta erutas = ruta.Obtener(nrepart.IdRuta);
            ViewBag.rutas = erutas;

            ViewBag.carpeta = archivos.Listar(accionUsr.IdAccion);

            if (accionUsr.FechaEntrega != null) ViewBag.fechaCadena = String.Format("{0:yyyy/MM/dd}", accionUsr.FechaEntrega);
            else ViewBag.fechaCadena = String.Format("{0:yyyy/MM/dd}", nrepart.ObtenerFechaServidor());

            return View(nrepart);
        }

        public ActionResult Imprimir(int id)
        {

            //ActionAsPdf result = new ActionAsPdf("PDFCliente", new { IDreparticion = id });
            
            //return result;
            Reparticion ereparticion = nreparticion.Obtener(id);
            ViewBag.reparticion = ereparticion;
            ViewBag.fechaActual = ereparticion.ObtenerFechaServidor();
            ViewBag.acciones = accion.Obtener(ereparticion.IdReparticion);
            return new ViewAsPdf(ruta.Obtener(ereparticion.IdRuta));
        }

        public ActionResult PDFCliente(int IDreparticion)
        {
            Reparticion ereparticion = nreparticion.Obtener(IDreparticion);
            ViewBag.reparticion = ereparticion;
            ViewBag.fechaActual = ereparticion.ObtenerFechaServidor();
            ViewBag.acciones = accion.Obtener(ereparticion.IdReparticion);
            return View(ruta.Obtener(ereparticion.IdRuta));
            
        }
        
        public JsonResult ActualizarAccion(Reparticion model, int codAccion, string FechaEntregaAccion, string EstadoAccion, string descripcion)
        {
            
            var rm = new ResponseModel();
            

            
            if (codAccion != null && FechaEntregaAccion !=null && EstadoAccion !=null)
            {
                Accion eaccion = accion.ObtenerPorIdAccion(codAccion);
                eaccion.FechaEntrega = Convert.ToDateTime(FechaEntregaAccion);
                eaccion.Descripcion = descripcion;
                eaccion.Estadp = EstadoAccion;
                Reparticion ereparticion = new Reparticion();
                if (EstadoAccion == "Cerrado")
                {
                    eaccion.FechaCierre = nreparticion.ObtenerFechaServidor();

                    //actualizar Reparticion
                    ereparticion = nreparticion.Obtener(eaccion.IdReparticion);
                    ereparticion.Estado = "Cerrado";

                    ereparticion.Guardar();

                    rm.SetResponse(false);
                }

                rm = eaccion.Guardar();

                if (rm.response)
                {
                    if (EstadoAccion == "Cerrado")
                    {
                        //rm.href = Url.Content("~/reparticion/Imprimir/" + ereparticion.IdReparticion);
                        
                    }
                    else rm.href = Url.Content("~/home/principal/");

                } 

            }
                           
            return Json(rm);
        }
	}
}