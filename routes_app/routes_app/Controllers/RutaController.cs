﻿using routes_app.Models;
using routes_app.filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa.MVC;

namespace routes_app.Controllers
{
    public class RutaController : Controller
    {
        //
        // GET: /Ruta/
        private Ruta hruta = new Ruta();
        private Reparticion ereparticion = new Reparticion();
        private Parametros parametro = new Parametros();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Guardar(Ruta model)
        {
            var rm = new ResponseModel();
            if (model.Estado == "Cerrado")
            {
                List<Reparticion> lista = ereparticion.ObtenerPendientes(model.IdRuta);
                if (lista.Count == 0)
                {
                    //no hay pendientes se puede cerrar
                    if (ModelState.IsValid)
                    {
                        rm = model.Guardar();

                        if (rm.response)
                        {
                            
                            //rm.href = Url.Content("~/ruta/EditarNuevaHojaRuta/" + model.IdRuta);
                            rm.href = Url.Content("~/home/SinReparticion/" + model.IdRuta);
                            
                        }

                    }
                }
                else
                {
                    rm.message = "No se pueden cerrar Hojas de Ruta con reparticiones <b>Pendientes.</b>";
                }

            }
            else
            {
                   
                if (ModelState.IsValid)
                {
                    rm = model.Guardar();

                    if (rm.response)
                    {

                        rm.href = Url.Content("~/home/SinReparticion/" + model.IdRuta);
                        //rm.href = Url.Content("~/ruta/EditarNuevaHojaRuta/" + model.IdRuta);
                    }

                }
           }
            
            return Json(rm);
        }
        [Autenticado]
        public ActionResult EditarNuevaHojaRuta(int Id)
        {
            ViewBag.reparticionListados = ereparticion.Listar(Id);
            Ruta rutas = hruta.Obtener(Id);
            return View(rutas);
        }

        public ActionResult ExportarPDF(int Id)
        {
            List<Reparticion> lreparticion = ereparticion.Listar(Id);
            ViewBag.listaReparticion = lreparticion;
            ViewBag.fechaActual = ereparticion.ObtenerFechaServidor();
            return new ViewAsPdf(hruta.Obtener(Id));            
        }

        public ActionResult PDF(int idruta)
        {
            List<Reparticion> lreparticion = ereparticion.Listar(idruta);
            ViewBag.listaReparticion = lreparticion;
            ViewBag.fechaActual = ereparticion.ObtenerFechaServidor();
            return View(hruta.Obtener(idruta));
            
        }
       
	}
}