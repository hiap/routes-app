﻿namespace routes_app.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("Parametros")]
    public partial class Parametros
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        
        [Key]
        public int IdParametro { get; set; }

        public int codRuta { get; set; }

        public string Obtener()
        {
            var nparametros = new Parametros();
            int codigo = new int();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    nparametros = ctx.Parametros.OrderBy(x => x.IdParametro)
                                                .SingleOrDefault();
                    codigo = nparametros.codRuta + 1;
                    
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Convert.ToString(codigo);
        }

        public ResponseModel Actualizar()
        {
            var rm = new ResponseModel();
            try
            {
                Parametros eparametros;
                using (var ctx = new RoutesContext())
                {
                    eparametros = ctx.Parametros.Where(x => x.IdParametro == 1)
                                                .SingleOrDefault();
                    eparametros.codRuta = (eparametros.codRuta + 1);

                    ctx.Entry(eparametros).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                rm.SetResponse(true);


            }

            catch (Exception er)
            {
                throw;
            }

            return rm;
        }
    }
}