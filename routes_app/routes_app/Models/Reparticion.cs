namespace routes_app.Models
{
    
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web;

    [Table("Reparticion")]
    public partial class Reparticion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Reparticion()
        {
            Accion = new HashSet<Accion>();
        }

        [Key]
        public int IdReparticion { get; set; }

        public int IdUsuario { get; set; }

        public int IdRuta { get; set; }
        [Required]
        public DateTime? FechaIngreso { get; set; }
        [Required]
        [StringLength(150)]
        public string CodUsrOrig { get; set; }

        [StringLength(150)]
        public string NomUsrOrig { get; set; }
        [Required]
        [StringLength(150)]
        public string CodUsrDest { get; set; }

        [StringLength(150)]
        public string NomUsrDest { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        [Column(TypeName = "text")]
        public string observacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Accion> Accion { get; set; }

        public virtual Ruta Ruta { get; set; }

        public virtual Usuario Usuario { get; set; }

        public Reparticion Obtener(int id)
        {
            var nrepart = new Reparticion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    nrepart = ctx.Reparticion.Where(x => x.IdReparticion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return nrepart;
        }

        public List<Reparticion> ObtenerPendientes(int codRuta)
        {
            var datos = new List<Reparticion>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    datos = ctx.Reparticion.Where(x => x.IdRuta == codRuta)
                                             .Where(x => x.Estado == "Pendiente")

                                        .ToList();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }
        public ResponseModel Guardar()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var ereparticion = ctx.Entry(this);

                    if (this.IdReparticion > 0) ereparticion.State = System.Data.Entity.EntityState.Modified;
                    else ereparticion.State = System.Data.Entity.EntityState.Added;

                    ctx.SaveChanges();

                    rm.SetResponse(true);



                }
            }
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }

        public DateTime ObtenerFechaServidor()
        {
            DateTime fecha = new DateTime();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    fecha = ctx.Database.SqlQuery<DateTime>("Select GETDATE() Fecha").SingleOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return fecha;
        }

        public List<Reparticion> Listar(int ID)
        {
            var datos = new List<Reparticion>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    datos = ctx.Reparticion.OrderBy(x => x.IdReparticion)
                                        .Where(x => x.IdRuta == ID)
                                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }
    }
}
