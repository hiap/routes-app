namespace routes_app.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RoutesContext : DbContext
    {
        public RoutesContext()
            : base("name=RoutesContext")
        {
        }

        public virtual DbSet<Accion> Accion { get; set; }
        public virtual DbSet<Archivos> Archivos { get; set; }
        public virtual DbSet<Instruccion> Instruccion { get; set; }
        public virtual DbSet<Permisos> Permisos { get; set; }
        public virtual DbSet<Reparticion> Reparticion { get; set; }
        public virtual DbSet<Ruta> Ruta { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Parametros> Parametros { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accion>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Accion>()
                .HasMany(e => e.Archivos)
                .WithRequired(e => e.Accion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Instruccion>()
                .HasMany(e => e.Accion)
                .WithRequired(e => e.Instruccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Parametros>();
                


            modelBuilder.Entity<Reparticion>()
                .HasMany(e => e.Accion)
                .WithRequired(e => e.Reparticion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ruta>()
                .Property(e => e.Referencia)
                .IsUnicode(false);

            modelBuilder.Entity<Ruta>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Ruta>()
                .HasMany(e => e.Reparticion)
                .WithRequired(e => e.Ruta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Permisos)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Reparticion)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.Ruta)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);
        }
    }
}
