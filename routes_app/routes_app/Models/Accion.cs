namespace routes_app.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("Accion")]
    public partial class Accion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Accion()
        {
            Archivos = new HashSet<Archivos>();
        }

        [Key]
        public int IdAccion { get; set; }

        public int IdRuta { get; set; }

        public int IdReparticion { get; set; }

        public int IdInstruccion { get; set; }

        public DateTime? FechaEntrega { get; set; }

        public DateTime? FechaCierre { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string Prioridad { get; set; }

        [StringLength(50)]
        public string Estadp { get; set; }

        public virtual Instruccion Instruccion { get; set; }

        public virtual Reparticion Reparticion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Archivos> Archivos { get; set; }

        public ResponseModel Guardar()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var eaccion = ctx.Entry(this);

                    if (this.IdAccion > 0) eaccion.State = System.Data.Entity.EntityState.Modified;
                    else eaccion.State = System.Data.Entity.EntityState.Added;

                    ctx.SaveChanges();

                    rm.SetResponse(true);



                }
            }
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }

        public Accion Obtener(int id)
        {
            var haccion = new Accion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    haccion = ctx.Accion.Where(x => x.IdReparticion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return haccion;
        }

        public Accion ObtenerPorIdAccion(int id)
        {
            var haccion = new Accion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    haccion = ctx.Accion.Where(x => x.IdAccion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return haccion;
        }
        public int ObtenerIdInstruccion(int id)
        {
            var naccion = new Accion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    naccion = ctx.Accion.Where(x => x.IdReparticion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return naccion.IdInstruccion;
        }

        public string ObtenerFechaCierre(int id)
        {
            var naccion = new Accion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    naccion = ctx.Accion.Where(x => x.IdReparticion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return Convert.ToString(naccion.FechaCierre);
        }

        public string ObtenerEstado(int id)
        {
            var naccion = new Accion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    naccion = ctx.Accion.Where(x => x.IdReparticion == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return naccion.Estadp;
        }
    }
}
