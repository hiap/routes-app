namespace routes_app.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("Instruccion")]
    public partial class Instruccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Instruccion()
        {
            Accion = new HashSet<Accion>();
        }

        [Key]
        public int IdInstruccion { get; set; }

        [StringLength(150)]
        public string NomInst { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Accion> Accion { get; set; }

        public List<Instruccion> Listar()
        {
            var datos = new List<Instruccion>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    datos = ctx.Instruccion.OrderBy(x => x.NomInst)
                                        .Where(x => x.Estado == "hab")
                                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        public string Obtener(int id)
        {
            var ninstruccion = new Instruccion();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    ninstruccion = ctx.Instruccion.Where(x => x.IdInstruccion == id)

                                        .SingleOrDefault();
                    if (ninstruccion == null)
                    {
                        ninstruccion = new Instruccion
                        {
                            NomInst="ERROR",
                            IdInstruccion = 0
                            
                        };
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return ninstruccion.NomInst;
        }
    }
}
