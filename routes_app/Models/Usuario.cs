namespace routes_app.Models
{
    using Helper;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity.Validation;
    using System.IO;
    using System.Linq;
    using System.Web;
    

    [Table("Usuario")]
    public partial class Usuario
    {
        private string HashHelper;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            Permisos = new HashSet<Permisos>();
            Reparticion = new HashSet<Reparticion>();
            Ruta = new HashSet<Ruta>();
        }

        [Key]
        public int IdUsuario { get; set; }

        [Required]
        [StringLength(50)]
        public string CI { get; set; }

        [Required]
        [StringLength(150)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(150)]
        public string Cargo { get; set; }

        [Required]
        [StringLength(80)]
        public string Cuenta { get; set; }

        [Required]
        [StringLength(80)]
        public string password { get; set; }

        [StringLength(150)]
        public string foto { get; set; }

         
        [StringLength(50)]
        public string estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Permisos> Permisos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reparticion> Reparticion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ruta> Ruta { get; set; }

        public ResponseModel Acceder(string cuenta, string password)
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    password = Helper.HashHelper.MD5(password);

                    var usuario = ctx.Usuario.Where(x => x.Cuenta == cuenta)
                                            .Where(x => x.password == password)
                                            .Where (x => x.estado == "hab")
                                            .SingleOrDefault();

                    if (usuario !=null)
                    {
                        SessionHelper.AddUserToSession(usuario.IdUsuario.ToString());
                        rm.SetResponse(true);
                    }
                    else
                    {
                        rm.SetResponse(false, "Cuenta o contraseņa incorrecta");
                    }
                }
            }
            catch (Exception)
            {
                throw;  
            }

            return rm;
        }
         
        public Usuario Obtener(int id)
        {
            var usuario = new Usuario();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    usuario = ctx.Usuario.Where(x => x.IdUsuario == id)
                                        
                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return usuario;
        }

        public string ObtenerCargo(int id)
        {
            var usuario = new Usuario();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    usuario = ctx.Usuario.Where(x => x.IdUsuario == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return usuario.Cargo;
        }

        public List<Usuario> Listar()
        {
            var datos = new List<Usuario>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    datos = ctx.Usuario.OrderBy(x => x.IdUsuario)
                                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        public ResponseModel Guardar(HttpPostedFileBase Foto)
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    ctx.Configuration.ValidateOnSaveEnabled = false;

                    var eusuario = ctx.Entry(this);

                    if (this.IdUsuario > 0) eusuario.State = System.Data.Entity.EntityState.Modified;
                    else eusuario.State = System.Data.Entity.EntityState.Added;
                                        
                    if (Foto != null)
                    {
                        string archivo = DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(Foto.FileName);

                        Foto.SaveAs(HttpContext.Current.Server.MapPath("~/img/demo/avatar/" + archivo));

                        this.foto = archivo;
                    }
                    else eusuario.Property(x => x.foto).IsModified = false;

                    if (this.password == null)
                    {
                        eusuario.Property(x => x.password).IsModified = false;
                    }
                    else this.password = Helper.HashHelper.MD5(this.password);

                        

                    ctx.SaveChanges();

                    rm.SetResponse(true);



                }
            }
            catch (DbEntityValidationException e)
            {
                throw;
            }
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }

        
    }
}
