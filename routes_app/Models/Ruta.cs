namespace routes_app.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web;

    [Table("Ruta")]
    public partial class Ruta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ruta()
        {
            Reparticion = new HashSet<Reparticion>();
        }

        [Key]
        public int IdRuta { get; set; }

        public int IdUsuario { get; set; }

        [Required]
        [StringLength(250)]
        public string Procedencia { get; set; }

        [Required]
        [Column(TypeName = "text")]
        public string Referencia { get; set; }

        [Required]
        public DateTime? FechaIngreso { get; set; }

        [Column(TypeName = "text")]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        [StringLength(150)]
        public string codruta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reparticion> Reparticion { get; set; }

        public virtual Usuario Usuario { get; set; }

        public List<Ruta> ObtenerRepetidos(string CodRuta)
        {
            var datos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {

                    datos = ctx.Ruta.Where(x => x.codruta == CodRuta).ToList();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        public Ruta Obtener(int id)
        {
            var hruta = new Ruta();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    hruta = ctx.Ruta.Where(x => x.IdRuta == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return hruta;
        }

        public ResponseModel Guardar()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var eruta = ctx.Entry(this);

                    if (this.IdRuta > 0) eruta.State = System.Data.Entity.EntityState.Modified;
                    else { 
                        eruta.State = System.Data.Entity.EntityState.Added;
                        Parametros param = new Parametros();
                        param.Actualizar();
                    }

                    ctx.SaveChanges();

                    rm.SetResponse(true);



                }
            }
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }

        public List<Ruta> Listar()
        {
            var datos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                   // datos = ctx.Ruta.OrderBy(x => x.FechaIngreso)
                                        
                     //                   .ToList();

                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0).Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta)).ToList();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        public List<Ruta> ListarPorFecha(DateTime fecha1, DateTime fecha2)
        {
            var datos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    // datos = ctx.Ruta.OrderBy(x => x.FechaIngreso)

                    //                   .ToList();

                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0 && t.FechaIngreso >= fecha1 && t.FechaIngreso <= fecha2).Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta) && x.FechaIngreso >= fecha1 && x.FechaIngreso<= fecha2)
                        
                        .ToList();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }
        public List<Ruta> ListarSinDerivaciones(DateTime fecha1, DateTime fecha2)
        {
            var datos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    //var contents = db.Content.SelectMany(c => c.TagRelation).Where(tr => relations.Contains(tr)).ToList();
                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0 && t.FechaIngreso >= fecha1 && t.FechaIngreso <= fecha2).Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => !relations.Contains(x.IdRuta) && x.FechaIngreso >= fecha1 && x.FechaIngreso <= fecha2).ToList();

                                  
                            
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        private bool TienePendientes(int Id)
        {
            bool Obtain = false;
            try
            {
                using (var ctx = new RoutesContext())
                {
                    //var contents = db.Content.SelectMany(c => c.TagRelation).Where(tr => relations.Contains(tr)).ToList();
                    var relations = ctx.Reparticion.Where(t => t.IdRuta == Id)
                                                    .Where(n => n.Estado == "Pendiente")
                                                    .Select(x => x.IdRuta).ToList();
                    if (relations.Count > 0)
                    {
                        Obtain = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Obtain;
        }

        public List<Ruta> PendienteCerrar()
        {
            var datos = new List<Ruta>();
            List<Ruta> newDatos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0)
                                                    .Where( n => n.Estado != "Pendiente")
                                                    .Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta))
                                    .Where(n => n.Estado == "Pendiente")
                                    .ToList();

                    foreach (var op in datos){
                        if (!TienePendientes(op.IdRuta))
                        {
                            newDatos.Add(op);
                        }
                    }



                }
            }
            catch (Exception)
            {
                throw;
            }

            return newDatos;
        }

        private bool NoTieneAccion(int Id)
        {
            bool Obtain = false;
            try
            {
                using (var ctx = new RoutesContext())
                {
                    //var contents = db.Content.SelectMany(c => c.TagRelation).Where(tr => relations.Contains(tr)).ToList();
                    var relations = ctx.Accion.Where(t => t.IdRuta == Id)
                                                    .Where(n => n.Descripcion == null)
                                                    .Select(x => x.IdRuta).ToList();
                    if (relations.Count > 0)
                    {
                        Obtain = true;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return Obtain;
        }
        public List<Ruta> PendienteLectura()
        {
            var datos = new List<Ruta>();
            List<Ruta> newDatos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0)
                                                    .Where(n => n.Estado == "Pendiente")
                                                    .Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta))
                                    .Where(n => n.Estado == "Pendiente")
                                    .ToList();

                    foreach (var op in datos)
                    {
                        if (NoTieneAccion(op.IdRuta))
                        {
                            newDatos.Add(op);
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return newDatos;
        }
        private bool NoTieneFechaApertura(int Id)
        {
            bool Obtain = false;
            try
            {
                using (var ctx = new RoutesContext())
                {
                    //var contents = db.Content.SelectMany(c => c.TagRelation).Where(tr => relations.Contains(tr)).ToList();
                    var relations = ctx.Accion.Where(t => t.IdRuta == Id)
                                                    .Where(n => n.FechaEntrega == null)
                                                    .Select(x => x.IdRuta).ToList();
                    if (relations.Count > 0)
                    {
                        Obtain = true;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return Obtain;
        }

        public List<Ruta> PendienteRespuesta()
        {
            var datos = new List<Ruta>();
            List<Ruta> newDatos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0)
                                                    .Where(n => n.Estado == "Pendiente")
                                                    .Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta))
                                    .Where(n => n.Estado == "Pendiente")
                                    .ToList();

                    foreach (var op in datos)
                    {
                        if (!NoTieneFechaApertura(op.IdRuta))
                        {
                            newDatos.Add(op);
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return newDatos;
        }

        public List<Ruta> RespuestaCerrada()
        {
            var datos = new List<Ruta>();
            List<Ruta> newDatos = new List<Ruta>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var relations = ctx.Reparticion.Where(t => t.IdRuta > 0)
                                                    .Where(n => n.Estado != "Pendiente")
                                                    .Select(x => x.IdRuta).ToList();

                    datos = ctx.Ruta.Where(x => relations.Contains(x.IdRuta))
                                    .ToList();

                    foreach (var op in datos)
                    {
                        if (!NoTieneFechaApertura(op.IdRuta))
                        {
                            newDatos.Add(op);
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return newDatos;
        }

        public class RutaMaestro
        {
            public DateTime? FechaIngreso { get; set; }
            public string codRuta { get; set; }
            public string Procedencia { get; set; }

            public string Referencia { get; set; }
            public string Usuario { get; set; }
            public string Estado { get; set; }
            public int IdReparticion { get; set; }
           

        }
        public List<RutaMaestro> ListarByReparticionPendientes(int Id)
        {
            List<RutaMaestro> datos = new List<RutaMaestro>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    IQueryable<Reparticion> oreparticion = ctx.Reparticion.Where(x => x.CodUsrDest == Id.ToString())
                                                                          .Where(x => x.Estado == "Pendiente");


                    foreach (var or in oreparticion)
                    {
                        int codUsrOrig = Convert.ToInt32(or.CodUsrOrig);
                        RutaMaestro rutamaestro = new RutaMaestro();
                        rutamaestro.FechaIngreso = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().FechaIngreso;
                        rutamaestro.Procedencia = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().Procedencia;
                        rutamaestro.Referencia = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().Referencia;
                        rutamaestro.Usuario = ctx.Usuario.Where(x => x.IdUsuario == codUsrOrig).SingleOrDefault().Nombre;
                        rutamaestro.Estado = ctx.Reparticion.Where(x => x.IdReparticion == or.IdReparticion).SingleOrDefault().Estado;
                        rutamaestro.codRuta = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().codruta;
                        rutamaestro.IdReparticion = or.IdReparticion;
                        datos.Add(rutamaestro);
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }

        
        public List<RutaMaestro> ListarByReparticion(int Id)
        {
            List<RutaMaestro> datos = new List<RutaMaestro>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    IQueryable<Reparticion> oreparticion = ctx.Reparticion.Where(x => x.CodUsrDest == Id.ToString());
                    

                    foreach (var or in oreparticion)
                    {
                        int codUsrOrig = Convert.ToInt32(or.CodUsrOrig);
                        RutaMaestro rutamaestro = new RutaMaestro();
                        rutamaestro.FechaIngreso = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().FechaIngreso;
                        rutamaestro.Procedencia = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().Procedencia;
                        rutamaestro.Referencia = ctx.Ruta.Where(x=> x.IdRuta == or.IdRuta).SingleOrDefault().Referencia;
                        rutamaestro.Usuario = ctx.Usuario.Where(x=> x.IdUsuario == codUsrOrig).SingleOrDefault().Nombre;
                        rutamaestro.Estado = ctx.Reparticion.Where(x => x.IdReparticion == or.IdReparticion).SingleOrDefault().Estado;
                        rutamaestro.codRuta = ctx.Ruta.Where(x => x.IdRuta == or.IdRuta).SingleOrDefault().codruta;
                        rutamaestro.IdReparticion = or.IdReparticion;
                        datos.Add(rutamaestro);
                    }

                    
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }
    }
}
