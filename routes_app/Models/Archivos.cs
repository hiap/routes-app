namespace routes_app.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.IO;
    using System.Linq;
    using System.Web;
    

    public partial class Archivos
    {
        [Key]
        public int IdArchivo { get; set; }

        public int IdAccion { get; set; }

        [StringLength(250)]
        public string Path { get; set; }

        [StringLength(150)]
        public string tipo { get; set; }

        [StringLength(50)]
        public string Extension { get; set; }

        public virtual Accion Accion { get; set; }

        public ResponseModel cargarArchivo(HttpPostedFileBase subirArchivo, Archivos model)
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var earchivos = ctx.Entry(model);

                    earchivos.State = System.Data.Entity.EntityState.Added;

                    if (subirArchivo != null)
                    {
                        string archivo = DateTime.Now.ToString("yyyyMMddHHmmss_") + model.IdAccion.ToString() + "_" + System.IO.Path.GetExtension(subirArchivo.FileName);

                        subirArchivo.SaveAs(HttpContext.Current.Server.MapPath("~/img/uploads/" + archivo));

                        model.Path = archivo;
                        model.Extension = System.IO.Path.GetExtension(subirArchivo.FileName);

                        ctx.SaveChanges();

                        rm.SetResponse(true);
                    }
                    else
                    {
                        rm.message = "No existe el archivo";
                    }

                }
            }
          
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }

        public List<Archivos> Listar(int Id)
        {
            var datos = new List<Archivos>();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    datos = ctx.Archivos.OrderBy(x => x.IdArchivo)
                                        .Where(x => x.IdAccion == Id )
                                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return datos;
        }


        public ResponseModel EliminarArchivo(Archivos model)
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    var earchivos = ctx.Entry(model);

                    earchivos.State = System.Data.Entity.EntityState.Deleted;
                    System.IO.File.Delete(HttpContext.Current.Server.MapPath("~/img/uploads/" + model.Path)); 
                    ctx.SaveChanges();

                    rm.SetResponse(true);
                }
                
            }

            catch (Exception er)
            {
                throw;
            }

            return rm;
        }
        public Archivos Obtener(int id)
        {
            var archivo = new Archivos();
            try
            {
                using (var ctx = new RoutesContext())
                {
                    archivo = ctx.Archivos.Where(x => x.IdArchivo == id)

                                        .SingleOrDefault();


                }
            }
            catch (Exception)
            {
                throw;
            }

            return archivo;
        }

    }
}
