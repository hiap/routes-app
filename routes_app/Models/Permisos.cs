namespace routes_app.Models
{
    using Helper;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Web;

    public partial class Permisos
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUsuario { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodUsr { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        public virtual Usuario Usuario { get; set; }

        public Permisos Obtener (int  id)
        {
            var permiso = new Permisos();
            try
            {
                using (var ctx = new RoutesContext() )
                {
                    permiso = ctx.Permisos.Where(x => x.IdUsuario == id)
                                          .SingleOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return permiso;
        }

        public ResponseModel Actualizar(int CodUsr, int idUsuario)
        {
            var rm = new ResponseModel();
            try
            {
                Permisos epermisos;
                var nuevoPermiso = new Permisos();
                using (var ctx = new RoutesContext())
                {
                    epermisos = ctx.Permisos.Where(x => x.IdUsuario == idUsuario)
                                                .SingleOrDefault();
                }
                if (epermisos != null)
                {
                    using (var ctxDelete = new RoutesContext())
                    {
                        ctxDelete.Entry(epermisos).State = System.Data.Entity.EntityState.Deleted;
                        ctxDelete.SaveChanges();
                    }
                } 
                

                nuevoPermiso.CodUsr = CodUsr;
                nuevoPermiso.IdUsuario = idUsuario;
                nuevoPermiso.Estado = "hab";

                using (var ctxNuevo = new RoutesContext())
                {
                    ctxNuevo.Permisos.Add(nuevoPermiso);
                    ctxNuevo.SaveChanges();
                }
                rm.SetResponse(true);
                

            }
         
            catch (Exception er)
            {
                throw;
            }

            return rm;
        }
    }
}
