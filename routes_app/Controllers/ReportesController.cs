﻿using Helper;
using routes_app.filters;
using routes_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace routes_app.Controllers
{
    public class ReportesController : Controller
    {

        private Ruta ruta = new Ruta();
        private Reparticion reparticion = new Reparticion();

        //
        // GET: /Reportes/
        public ActionResult Index()
        {
            return View();
        }
        

        [Autenticado]
        public ActionResult PendientesCerrar()
        {
            ViewBag.rutasListado = ruta.PendienteCerrar();
            return View();
        }

        [Autenticado]
        public ActionResult PendientesLectura()
        {
            ViewBag.rutasListado = ruta.PendienteLectura();
            return View();
        }

        [Autenticado]
        public ActionResult PendientesRespuesta()
        {
            ViewBag.rutasListado = ruta.PendienteRespuesta();
            return View();
        }

        [Autenticado]
        public ActionResult Ingresos()
        {
            
            return View();
        }

         [Autenticado]
        
        public ActionResult IngresosResultado(string fecha1, string fecha2)
        {
             if (fecha1!= null && fecha2 != null)
             {
                 DateTime fec1 = Convert.ToDateTime(fecha1 + " 00:00:00");
                 DateTime fec2 = Convert.ToDateTime(fecha2 + " 23:59:59");

                 List<routes_app.Models.Reparticion.ReparticionMaestro> rep = reparticion.ObtenerIngresos(fec1, fec2, "Pendiente");

                 ViewBag.reparticionListado = rep;
                 ViewBag.fechaInicial = fec1;
                 ViewBag.fechaFinal = fec2;

                 return View();
             }
             else
             {
                 return Redirect("~/reportes/ingresos");
             }
            
        }

         [Autenticado]
         public ActionResult Salidas()
         {
             
             return View();
         }

         [Autenticado]

         public ActionResult SalidasResultado(string fecha1, string fecha2)
         {
             if (fecha1 != null && fecha2 != null)
             {
                 DateTime fec1 = Convert.ToDateTime(fecha1 + " 00:00:00");
                 DateTime fec2 = Convert.ToDateTime(fecha2 + " 23:59:59");

                 List<routes_app.Models.Reparticion.ReparticionMaestro> rep = reparticion.ObtenerSalidas(fec1, fec2, "Cerrado");

                 ViewBag.reparticionListado = rep;
                 ViewBag.fechaInicial = fec1;
                 ViewBag.fechaFinal = fec2;

                 return View();
             }
             else
             {
                 return Redirect("~/reportes/salidas");
             }

         }

	}
}