﻿using Helper;
using routes_app.filters;
using routes_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace routes_app.Controllers
{
    
    public class HomeController : Controller
    {
        private Usuario usuario = new Usuario();
        private Permisos permiso = new Permisos();
        private Parametros parametro = new Parametros();
        private Ruta ruta = new Ruta();
        private Reparticion reparticion = new Reparticion();
        
        //
        // GET: /Home/
        [NoLogin]
        public ActionResult Index()
        {
           
            return View();
        }

        public JsonResult Acceder(string cuenta, string password)
        {
            var rm = usuario.Acceder(cuenta, password);
            if (rm.response)
            {
                rm.href = Url.Content("~/home/principal");
            }

            return Json(rm);
        }

        [Autenticado]
        public ActionResult Principal()
        {
            ViewBag.listaMaestro = ruta.ListarByReparticionPendientes(Helper.SessionHelper.GetUser());
            return View();
        }
        [Autenticado]
        public ActionResult Historicos()
        {
            ViewBag.listaMaestro = ruta.ListarByReparticion(Helper.SessionHelper.GetUser());
            return View();
        }
        [Autenticado]
        public ActionResult EditarHR()
        {
            return View();
        }
        [Autenticado]
        public ActionResult EditarInstruccion()
        {
            return View();
        }
        [Autenticado]
        public ActionResult NuevaHojaRuta()
        {
            Ruta hruta = new Ruta();
            hruta.IdUsuario = Helper.SessionHelper.GetUser();
            hruta.Estado = "Pendiente";
            hruta.codruta = parametro.Obtener();
            hruta.FechaIngreso = reparticion.ObtenerFechaServidor();

            return View(hruta);
        }
        [Autenticado]
        public ActionResult NuevaReparticion(int Id)
        {
            Ruta rutaSeleccionada = ruta.Obtener(Id);
            ViewBag.codRuta = rutaSeleccionada.codruta;
            Reparticion nreparticion = new Reparticion();
            nreparticion.IdRuta = Id;
            nreparticion.IdUsuario = Helper.SessionHelper.GetUser();
            nreparticion.FechaIngreso = reparticion.ObtenerFechaServidor();
            ViewBag.UsuariosListados = usuario.Listar();
            return View(nreparticion);
        }
        [Autenticado]
        public ActionResult EditarInstruccionDireccion()
        {
            return View();
        }
        [Autenticado]
        public ActionResult VerificarEstado(string fechaInicio, string fechaFin)
        {
            //ViewBag.rutasListado = ruta.Listar();
            DateTime fec2=DateTime.Now;
            DateTime fec1=DateTime.Now;
            if (fechaInicio == null)
            {
                
                fec1 = fec2.AddMonths(-1);
            }else
            {
                fec1 = Convert.ToDateTime(fechaInicio + " 00:00:00");
                fec2 = Convert.ToDateTime(fechaFin + " 23:59:59");
            }
            
            
            ViewBag.rutasListado = ruta.ListarPorFecha(fec1, fec2);
            return View();
        }
        [Autenticado]
        public ActionResult SinReparticion(string fechaInicio, string fechaFin)
        {
            DateTime fec2 = DateTime.Now;
            DateTime fec1 = DateTime.Now;
            if (fechaInicio == null)
            {

                fec1 = fec2.AddMonths(-1);
            }
            else
            {
                fec1 = Convert.ToDateTime(fechaInicio + " 00:00:00");
                fec2 = Convert.ToDateTime(fechaFin + " 23:59:59");
            }
            ViewBag.rutasListado = ruta.ListarSinDerivaciones(fec1, fec2);
            return View();
        }
        [Autenticado]
        public ActionResult Usuarios()
        {
            ViewBag.UsuariosListados = usuario.Listar();
            return View();
        }
        [Autenticado]
        public ActionResult NuevoUsuario()
        {
            Usuario usr = new Usuario();
            return View(usr);
        }
        
        public ActionResult Logout()
        {
            SessionHelper.DestroyUserSession();
            return Redirect("~/Home/Index");            
        }
        [Autenticado]
        
        public ActionResult EditarUsuario(int Id)
        {
            ViewBag.Permiso = permiso.Obtener(Id);
            ViewBag.Nivel = permiso.Obtener(SessionHelper.GetUser());
            Permisos per = permiso.Obtener(SessionHelper.GetUser());
            Usuario usr = usuario.Obtener(Id);
            if ((Id != SessionHelper.GetUser()) && (per.CodUsr != 1))
            {
                return Redirect("~/Home/Principal");  
            }

            if (usr != null)
            {
                
                return View(usr);
            }
            else return Redirect("~/Home/Principal");  

            
        }
	}
}